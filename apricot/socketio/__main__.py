import apricot
import socketio
import asyncio
import logging
import eventlet

logger = logging.getLogger('apricot')

sio = socketio.Server()
app = socketio.WSGIApp(sio)

class SocketioBackendWrapper:

    def __init__(self,
            address,
            ):
        general_args = {
                'outflow': self.outflow,
                'session_factory': apricot.cache.get_session_factory(),
                }

        self.address = address

        self.backend = apricot.backend.Backend.from_address(
               address = self.address,
               )(**general_args)

    def lookup(self):
        return self.backend.lookup(self.address)

    def outflow(self, method, **kwargs):
        sio.emit(method, kwargs)

@sio.event
def lookup(sid, data):
    logger.info('Lookup event: %s', data)
    sibw = SocketioBackendWrapper(address = data['address'])
    sibw.lookup()

def main():
    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)

if __name__=='__main__':
    main()
