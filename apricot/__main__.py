import apricot
import argparse
import textwrap

# TODO the printing code needs refactoring

def print_headers(headers, indent):
    for k,v in headers.items():
        for line in textwrap.wrap(
                f"{k}: {v}",
                initial_indent = ' '*indent,
                subsequent_indent = ' '*(len(k)+indent+2),
                ):
            print(line)

def main():
    parser = argparse.ArgumentParser(
            prog = 'apricot',
            description=(
                'look stuff up over ActivityPub '
                '(and related protocols)'
                ),
            )

    apricot.cache.set_up_parser(parser)

    parser.add_argument(
            'address',
            help='address to look up')
    parser.add_argument(
            '--verbose', '-v', action='store_true',
            help='show what we send and what we receive')

    args = parser.parse_args()

    def console_outflow(method, **kwargs):

        if method == 'results':
            print(f'👉 results:')
            print_headers(kwargs, 5)
        elif method == 'received':
            cached = kwargs.get('cached', False)
            if cached:
                print(f'🗃️ received (from cache):')
            else:
                print(f'📡 received (from network):')
            print_headers(kwargs['headers'], 2)
            if kwargs.get('body', None) is not None:
                print()
                for line in textwrap.wrap(
                        kwargs['body'],
                        initial_indent = '  ',
                        subsequent_indent = '  ',
                        ):
                    print(line)

        elif method == 'sent':
            print(f'📣 sent:')
            print_headers(kwargs['headers'], 2)
            print(f'  {kwargs["verb"]} {kwargs["url"]}')
            print()
        elif method in ['status']:
            for line in textwrap.wrap(
                    kwargs['message'],
                    initial_indent = 'ℹ️  ',
                    subsequent_indent = '   ',
                    ):
                print(line)

        elif method in ['warning']:
            for line in textwrap.wrap(
                    kwargs['message'],
                    initial_indent = '⚠️  ',
                    subsequent_indent = '   ',
                    ):
                print(line)

        else:
            print(f"  -- unknown message type {method}! Args were: {kwargs}")

    general_args = {
            'outflow': console_outflow,
            'session_factory': apricot.cache.get_session_factory(args),
            }

    backend = apricot.backend.Backend.from_address(
            address = args.address,
            )(**general_args)

    backend.lookup(
            address = args.address,
            )

if __name__=='__main__':
    main()
