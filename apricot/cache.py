import os
import requests
import requests_cache

DEFAULT_PEON_CACHE_NAME = os.path.join(
        '~/.cache/apricot/',
        requests_cache.DEFAULT_CACHE_NAME,
        )
DEFAULT_ROOT_CACHE_NAME = os.path.join(
        '/var/cache/apricot/',
        requests_cache.DEFAULT_CACHE_NAME,
        )

def _default_cache_name():
    if os.getpid()==0:
        return DEFAULT_ROOT_CACHE_NAME
    else:
        return os.path.expanduser(DEFAULT_PEON_CACHE_NAME)

def set_up_parser(parser):
    parser.add_argument(
            '--cache-file',
            help=(
                f'location of cache (defaults to {DEFAULT_ROOT_CACHE_NAME} '
                f"if you're root, {DEFAULT_PEON_CACHE_NAME} otherwise)"
                ),
            default=None,
            )
    parser.add_argument(
            '--no-cache', '-C',
            action='store_true',
            help='run without a cache',
            )
    return parser

def session_factory_with_no_cache(*args, **kwargs):
    # This is at top level so that the tests can use it.
    return requests.Session(
            *args, **kwargs)

def get_session_factory(parsed_args=None):


    cache_name = (
            (parsed_args is not None and parsed_args.cache_file)
            or _default_cache_name()
            )

    os.makedirs(cache_name, exist_ok = True)

    def session_factory_with_cache(*args, **kwargs):
        return requests_cache.CachedSession(
                cache_name,
                *args, **kwargs)

    if parsed_args is not None and parsed_args.no_cache:
        return session_factory_with_no_cache
    else:
        return session_factory_with_cache

__all__ = [
    'set_up_parser',
    'get_session_factory',
    ]
