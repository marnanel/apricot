import re
from apricot.backend.backend import Backend, BackendError
from apricot.backend.constants import *

# see https://github.com/jhass/nodeinfo/blob/main/PROTOCOL.md
# and https://github.com/jhass/nodeinfo/blob/main/README.md

class Nodeinfo(Backend):

    def lookup(self, address):

        # FIXME check hostname is valid

        self.outflow(method='status', wait=True,
                     message='Sending request for nodeinfo')

        r = self.request('GET',
                         f'https://{address}/.well-known/nodeinfo',
                         accept='application/jrd+json',
                         reason='nodeinfo-find',
                         )

        # FIXME: non-json

        nodeinfo_spec_re = re.compile(
            r'http://nodeinfo.diaspora.software/ns/schema/(\d+\.\d)$')
        nodeinfo_url = None
        nodeinfo_version = None

        for link in r.json()['links']:
            match = re.match(nodeinfo_spec_re, link['rel'])
            if not match:
                continue
            nodeinfo_version = match.group(1)
            nodeinfo_url = link['href']

        nodeinfo_request = self.request('GET',
                                        nodeinfo_url,
                                        accept = 'application/json',
                                        reason = 'nodeinfo',
                                        )

        nodeinfo = nodeinfo_request.json()

        # FIXME nodeinfo['version'] must match

        results = {}

        results['codebase'] = nodeinfo['software']['name']
        results['version'] = nodeinfo['software']['version']
        results['protocols'] = nodeinfo['protocols']
        results['inbound'] = nodeinfo['services']['inbound']
        results['outbound'] = nodeinfo['services']['outbound']
        results['usercount'] = nodeinfo['usage']['users']['total']
        results['month'] = nodeinfo['usage']['users']['activeMonth']
        results['halfyear'] = nodeinfo['usage']['users']['activeHalfyear']
        results['posts'] = nodeinfo['usage']['localPosts']
        results['open'] = nodeinfo['openRegistrations']
        results['metadata'] = nodeinfo['metadata']

        self.outflow(method='results', **results)
