import re
import requests

class Backend:

    def __init__(self,
                 outflow,
                 session_factory,
                 ):
        self.outflow = staticmethod(outflow).__func__
        self.session = session_factory()

    def lookup(self, address):
        raise NotImplementedError()

    @property
    def cached(self):
        return type(self.session)!=requests.Session

    def warn(self, message):
        self.outflow(method = 'warning',
                     message = message,
                     )


    def request(self, method, url,
                accept,
                reason,
                expect_status = 200,
                **kwargs):

        # FIXME drop back to http

        kwargs['headers'] = kwargs.get('headers', {}) | {
                'User-Agent': 'Apricot (https://marnanel.org/apricot/)',
                'Accept': accept,
                }
        prepared = requests.Request(
                method = method,
                url = url,
                **kwargs).prepare()
        self.outflow(method='sent',
                     headers = dict(prepared.headers),
                     body = prepared.body,
                     verb = prepared.method,
                     url = url,
                     reason = reason,
                )
        response = self.session.send(prepared)
        self.outflow(method='received',
                     headers = dict(response.headers),
                     body = response.text,
                     reason = reason,
                     cached = self.cached,
                     )

        if expect_status is not None and response.status_code!=expect_status:
            raise BackendError(
                    f"Expected status {expect_status} "
                    f"but received {response.status_code}.")

        return response

    @classmethod
    def from_another(cls, another):
        
        if cls==Backend:
            raise NotImplementedError()

        result = cls.__new__(cls)
        result.session = another.session
        result.results = None
        
        def outflow_except_results(method, **kwargs):
            if method=='results':
                result.results = kwargs
            else:
                another.outflow(method, **kwargs)

        result.outflow = outflow_except_results

        return result

    @classmethod
    def from_address(cls, address):
        if address.startswith('https://'):
            from apricot.backend.activitypub import ActivityPub
            return ActivityPub
        elif re.search(r'^(acct:)?@?[A-Za-z0-9._-]+@[A-Za-z0-9.]+$',
                       address):
            from apricot.backend.webfinger import Webfinger
            return Webfinger
        elif re.search('^[A-Za-z0-9.-]+$',
                       address):
            from apricot.backend.nodeinfo import Nodeinfo
            return Nodeinfo
        else:
            raise ValueError(
                    "Please give an address in one of these forms:\n"
                    " - https://hostname - ActivityPub lookup\n"
                    " - user@hostname    - webfinger lookup\n"
                    " - hostname         - nodeinfo lookup\n"
                    )

class BackendError(ValueError):
    pass

__all__ = [
        'Backend',
        'BackendError',
        ]
