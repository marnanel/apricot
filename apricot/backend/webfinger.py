from apricot.backend.backend import Backend, BackendError
from apricot.backend.host import Host
from apricot.backend.constants import *

class Webfinger(Backend):

    def lookup(self, address):

        if address.startswith('acct:'):
            address = address[5:]

        if address.startswith('@'):
            address = address[1:]

        fields = address.split('@')
        if len(fields)!=2:
            raise BackendError("Format is username@host")

        username, hostname = fields

        # FIXME check hostname is valid

        host_meta = Host.from_another(self)
        host_meta.lookup(
                address = hostname,
                )
        self.outflow(method='status', wait=True, message='Sending request')

        template = host_meta.results['webfinger']

        # FIXME check template is on the same host

        user_webfinger_url = template.replace('{uri}',
                                              f'{username}@{hostname}')

        r = self.request('GET', user_webfinger_url,
                         accept='application/jrd+json',
                         reason='webfinger',
                         )

        # FIXME: non-json
        # FIXME: check against webfinger spec
        # FIXME: check types
        found = r.json()

        results = {
                'html-profile': None,
                'activitypub': None,
                }

        results['subject'] = found['subject']
        results['alias'] = found['aliases']

        for link in found['links']:
            if link['rel']=='http://webfinger.net/rel/profile-page':
                results['html-profile'] = link['href']
            elif link['rel']=='self':
                if link['type']=='application/activity+json':
                    results['activitypub'] = link['href']

        self.outflow(method='results', **results)
