from apricot.backend.backend import Backend
from apricot.backend.constants import *
import json

class ActivityPub(Backend):

    def lookup(self, address):
        self.outflow(method='status', message='Sending request')

        # FIXME Check we have webfinger for that host

        r = self.request('GET', address,
                         accept='application/activity+json',
                         reason='activitypub',
                         )

        results = {}
        found = r.json()

        results |= found

        if '@context' in results:
            del results['@context']
        else:
            self.warn('There was no @context value in this object.')

        if 'id' in results:
            if results['id'] != address:
                self.warn(
                        f'The URL we fetched was {address}, but '
                        f'the object claims to be {results["id"]}.'
                        )

        if 'url' in results:
            if results['url'] != address:
                self.warn(
                        f'The URL we fetched was {address}, but '
                        f'the object claims to be at {results["url"]}.'
                        )

        if 'icon' in results:
            if results['icon']['type']=='Image':
                results['icon'] = results['icon']['url']

        self.outflow(method='results', **results)
