from bs4 import BeautifulSoup
from apricot.backend.backend import Backend
from apricot.backend.constants import *

class Host(Backend):

    def lookup(self, address):
        self.outflow(method='status', message='Sending request')

        url = f'https://{address}/.well-known/host-meta'
        r = self.request('GET', url,
                         accept='application/xml',
                         reason='host-meta',
                         )

        results = {}

        soup = BeautifulSoup(r.text, 'xml')
        link_tag = soup.find('Link', rel='lrdd')
        if link_tag is None:
            raise BackendError("There was a host-meta.xml, but it doesn't "
                               "specify a webfinger service.")

        link = link_tag.get('template', None)
        if link is None:
            raise BackendError("The webfinger link is missing a template.")

        results['webfinger'] = link

        self.outflow(method='results', **results)
