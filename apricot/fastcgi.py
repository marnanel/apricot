from socketserver import TCPServer
from fastcgi.core import FcgiHandler
import argparse
from json import dumps
import logging
import urllib.parse

logger = logging.getLogger('apricot.fastcgi')

TCPServer.allow_reuse_address = True

# environ:
# query {'QUERY_STRING': '', 'REQUEST_METHOD': 'GET', 'CONTENT_TYPE': '', 'CONTENT_LENGTH': '', 'SCRIPT_NAME': '/', 'REQUEST_URI': '/', 'DOCUMENT_URI': '/', 'DOCUMENT_ROOT': '/var/www/html', 'SERVER_PROTOCOL': 'HTTP/1.1', 'REQUEST_SCHEME': 'https', 'HTTPS': 'on', 'GATEWAY_INTERFACE': 'CGI/1.1', 'SERVER_SOFTWARE': 'nginx/1.22.1', 'REMOTE_ADDR': '217.155.192.32', 'REMOTE_PORT': '65482', 'REMOTE_USER': '', 'SERVER_ADDR': '192.168.1.83', 'SERVER_PORT': '443', 'SERVER_NAME': 'sandy-heath.thurman.org.uk', 'REDIRECT_STATUS': '200', 'HTTP_HOST': 'sandy-heath.thurman.org.uk', 'HTTP_USER_AGENT': 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/115.0', 'HTTP_ACCEPT': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8', 'HTTP_ACCEPT_LANGUAGE': 'en-GB,en;q=0.5', 'HTTP_ACCEPT_ENCODING': 'gzip, deflate, br', 'HTTP_CONNECTION': 'keep-alive', 'HTTP_COOKIE': 'csrftoken=Ga1NZdJTo0F5zaSXCmmYq69xSePMfzIk', 'HTTP_UPGRADE_INSECURE_REQUESTS': '1', 'HTTP_SEC_FETCH_DEST': 'document', 'HTTP_SEC_FETCH_MODE': 'navigate', 'HTTP_SEC_FETCH_SITE': 'cross-site', 'HTTP_SEC_GPC': '1'}

CONTENTTYPE_ACTIVITY = 'application/activity+json'
CONTENTTYPE_HTML = 'text/html'
CONTENTTYPE_HOST_META = 'application/xrd+xml'
CONTENTTYPE_NODEINFO = (
        'application/json; '
        'profile=http://nodeinfo.diaspora.software/ns/schema/2.0#'
        )

USER_RE = r'/users/([a-z0-9-]+)/?'
HOST_META_URI = '/.well-known/host-meta'
NODEINFO_PART_1_URI = '/.well-known/nodeinfo'
NODEINFO_PART_2_URI = '/nodeinfo.json'

WEBFINGER_URI = '/.well-known/webfinger'
WEBFINGER_MIMETYPE = 'application/jrd+json; charset=utf-8'

DEFAULT_HOST = 'localhost'
DEFAULT_PORT = 17177

def fastcgi_command(subparsers):
    parser = subparsers.add_parser('fastcgi')

    parser.add_argument(
            '--port', '-P',
            default=DEFAULT_PORT,
            type=int,
            help=(
                f'the port to listen on (default {DEFAULT_PORT})'
            ),
            )

    parser.add_argument(
            '--host', '-H',
            default=DEFAULT_HOST,
            help=(
                f'the host to listen on (default {DEFAULT_HOST})'
            ),
            )

    parser.add_argument(
            '--verbose', '-v',
            action='store_true',
            help=(
                'print more information'
            ),
            )

def _encode(s):
    return s.replace('\n','\r\n').encode('UTF-8')

def despatch_user(env, match):
    result = {
            'mimetype': CONTENTTYPE_ACTIVITY,
            }
    raise NotImplementedError()
    return result

def despatch_host_meta(env, match):

    username = match.groups(1)

    result = {
        'mimetype': CONTENTTYPE_HOST_META,
        'text': f"""<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
        <Link rel="lrdd" type="application/xrd+xml"
        template="https://{env['SERVER_NAME']}/.well-known/webfinger?resource={{uri}}"/>
</XRD>""",
}

    return result

def despatch_nodeinfo_part_1(env, match):

    result = {
            'mimetype': CONTENTTYPE_NODEINFO,
            'json': {
                'links': [
                    {
                        "rel": "http://nodeinfo.diaspora.software/ns/schema/2.0",
                        "href": f"https://{env['SERVER_NAME']}/nodeinfo.json",
                        },
                    ],
                },
            }

    return result

def despatch_nodeinfo_part_2(env, match):
    result = {
            'mimetype': CONTENTTYPE_NODEINFO,
            'json': {
                "version": "2.0",
                "software" : {
                    "name": "apricot",
                    "version": apricot.__version__,
                    },
                "protocols": ['activitypub'],
                "services": {
                    "inbound": [],
                    "outbound": [],
                    },
                "openRegistrations": False,
                "usage": {
                    "users": {
                        # When this information is meaningful,
                        # we can implement this more seriously.
                        "total": 1,
                        "activeMonth": 1,
                        },
                    "localPosts": 0,
                    "localComments": 0,
                    },
                "metadata": {
                    },
                },
            }

    return result

def _despatch_webfinger_inner(env, match):

    username = urllib.parse.parse_qs(
            qs = env['QUERY_STRING'],
            ).get('resource', '')

    if not username:
        return {
                'status': 400,
                'reason': 'No resource specified for webfinger',
                }
    username = username[0]

    # Generally, username resources should be prefaced with "acct:",
    # per RFC7565. We support this, but we don't enforce it.
    username = re.sub(r'^acct:', '', username)

    
    if '@' not in username:
        return {
                'status': 400,
                'reason': 'Absolute name required',
                }

    user_part, host_part = username.split('@', 2)

    if host_part not in [
            env['SERVER_NAME'],
            ]:
        return {
                'status': 404,
                'reason': 'Not local',
                }

    user = config.users[user_part]


    if not user.exists:
        return {
                'status': 404,
                'reason': 'Not known',
                }


    user_url = normalise_url(
            address = config.user_uri % {'username':user_part},
            env = env,
            )

    try:
        atom_uri = config.atom_uri
    except AttributeError:
        atom_uri = None

    result = {
            'mimetype': WEBFINGER_MIMETYPE,
            'json': {
                "subject" : f"acct:{user_part}@{host_part}",
                "aliases" : [
                    user_url,
                    ],

                "links":[
                    {
                        'rel': 'http://webfinger.net/rel/profile-page',
                        'type': 'text/html',
                        'href': user_url,
                        },
                    {
                        'rel': 'self',
                        'type': 'application/activity+json',
                        'href': user_url,
                        },
                    {
                        'rel': 'http://ostatus.org/schema/1.0/subscribe',
                        'template': normalise_url(
                            address = config.authorise_follow_uri,
                            env=env,
                            ),
                        },
                    ]},
                }

    if atom_uri is not None:
        result['json']['links'].append(
                {
                    'rel': 'http://schemas.google.com/g/2010#updates-from',
                    'type': 'application/atom+xml',
                    'href': normalise_url(
                        address = atom_uri,
                        env = env,
                        ),
                    },
                )


    return result

def despatch_webfinger(env, match):
    result = _despatch_webfinger_inner(env, match)
    result['extra_headers'] = {
            'Access-Control-Allow-Origin': '*',
            }
    return result

def format_message_for_http(
        mimetype = 'text/plain',
        status = 200,
        reason = 'OK',
        text = None,
        json = None,
        extra_headers = {},
        ):

    if json is not None:
        assert text is None
        body = dumps(json, indent=2, sort_keys=True)
    elif text is not None:
        body = text
    else:
        assert mimetype=='text/plain'
        body = reason

    body = body.encode('UTF-8')

    headers = (
            f"Content-Type: {mimetype}\r\n"
            f"Status: {status} {reason}\r\n"
            f"Content-Length: {len(body)}\r\n"
            )

    for f,v in extra_headers.items():
        headers += f'{f}: {v}\r\n'

    headers += "\r\n"

    headers = headers.encode('UTF-8')

    return headers+body

def despatch(env):
    # XXX Here we check HTTP_ACCEPT
    # XXX and DOCUMENT_URI and possibly QUERY_STRING
    # XXX and despatch as appropriate

    logger.debug('query: %s', env)

    uri = env['DOCUMENT_URI']

    for regex, handler in [
            (USER_RE, despatch_user),
            (HOST_META_URI, despatch_host_meta),
            (NODEINFO_PART_1_URI, despatch_nodeinfo_part_1),
            (NODEINFO_PART_2_URI, despatch_nodeinfo_part_2),
            (WEBFINGER_URI, despatch_webfinger),
            ]:

        match = re.match(regex, uri)
        if match is None:
            continue

        result = handler(
                env = env,
                match = match,
                )

        if result is not None:
            result = format_message_for_http(**result)
            return result

    return format_message_for_http(
            status = 404,
            )

class ApricotHandler(FcgiHandler):

    def handle(self):
        logger.debug('query: %s', self.environ)
        result = 'hello world'

        self['stdout'].write(_encode(result))

def run():

    parser = argparse.ArgumentParser(
        description='server side of ActivityPub browser')
    parser.add_argument(
        '--verbose', '-v', action='store_true',
        help='show more information')
    parser.add_argument(
        '--host', '-H', type=str, default=DEFAULT_HOST,
        help='host to listen on')
    parser.add_argument(
        '--port', '-P', type=int, default=DEFAULT_PORT,
        help='port to listen on')
    args = parser.parse_args()

    logging.basicConfig(encoding='utf-8')

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)


    with TCPServer((args.host, args.port), ApricotHandler) as srv:
        logger.info("Listening at %s:%s",
                    DEFAULT_HOST, DEFAULT_PORT)
        srv.handle_request()

if __name__=='__main__':
    run()
