#!/usr/bin/env python
import subprocess
import sys
import os

LOGS_DIRECTORY = '/var/log/apricot'
SOCKET_FILE = '/var/run/web/apricot.sock'
LOG_LEVEL = 'debug'

def main():

    if os.getcwd().endswith('/bin'):
        gunicorn_binary = '../venv/bin/gunicorn'
    else:
        gunicorn_binary = 'venv/bin/gunicorn'

    gunicorn_binary = os.path.abspath(gunicorn_binary)

    if not os.path.exists(gunicorn_binary):
        print("Can't find f{gunicorn_binary}.")
        sys.exit(255)

    args = [
            gunicorn_binary,
            '--worker-class', 'eventlet',
            '--workers', '1', # gunicorn requires a single worker
            '--bind', f'unix:{SOCKET_FILE}',
            ]

    if LOGS_DIRECTORY is not None:
        args.extend([
            '--access-logfile', f'{LOGS_DIRECTORY}/apricot.access.log',
            '--error-logfile', f'{LOGS_DIRECTORY}/apricot.error.log',
            '--disable-redirect-access-to-syslog',
            ])
    
    if LOG_LEVEL is not None:
        args.extend([
            '--log-level', f'{LOG_LEVEL}',
            ])

    args.extend([
        f'apricot.wsgi:app',
        ])

    called = subprocess.run(
            args = args,
            )

    if called.returncode!=0:
        print(f"warning: call returned {called.returncode}")

    sys.exit(called.returncode)

if __name__=='__main__':
    main()
