function show_raw(block, line) {
    console.log("Raw values for "+block+": "+line);
    if (block!='sent' && block!='headers' && block!='body') {
        console.log("  -- which is not a real block; ignoring");
        return;
    }

    $('.'+block).text(line);
}

$(document).ready(function() {
    $('#go').click(function() {

        $('#results tr:last').after('<tr><td>ooh</td></tr>');
    });

    var socket = io("https://marnanel.org", {
        path: '/apricot/endpoint'
    });
    console.log(socket);

    socket.on('connect', function(){
        console.log('connect');
        socket.emit("lookup",
                {'address': "queer.party"},
        );
        console.log('sent');

    });
    socket.on('raw', (stuff) => {
        console.log('....raw?');
        show_raw(stuff[0], stuff[1]);
    });
    socket.on('status', (stuff) => {
        console.log('....status');
        console.log(stuff);
    });

});

