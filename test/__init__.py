METHODS = [
        'status', 
        'sent',
        'received',
        'results',
        'admonition',
        ]


EXAMPLE_HOST_META = """<?xml version="1.0" encoding="UTF-8"?>
<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">
  <Link rel="lrdd"
  template="https://altair.example.com/.well-known/webfinger?resource={uri}"/>
</XRD>"""

# https://github.com/jhass/nodeinfo/blob/main/PROTOCOL.md
EXAMPLE_NODEINFO_1 = """{"links":[{"rel":
"http://nodeinfo.diaspora.software/ns/schema/2.0",
"href":"https://altair.example.com/nodeinfo/2.0"}]}"""

EXAMPLE_NODEINFO_2 = """{"version":"2.0","software":{"name":"mastodon",
"version":"4.1.8+trans_rights-0aef33b-23262162413"},"protocols":[
"activitypub"],"services":{"outbound":[],"inbound":[]},"usage":{
"users":{"total":6145,"activeMonth":497,"activeHalfyear":853},
"localPosts":1003581},"openRegistrations":false,"metadata":{}}"""

EXAMPLE_WEBFINGER = """{"subject":"acct:alice@altair.example.com",
"aliases":["https://altair.example.com/@alice",
"https://altair.example.com/users/alice"],
"links":[{"rel":"http://webfinger.net/rel/profile-page",
"type":"text/html","href":"https://altair.example.com/@alice"},
{"rel":"self","type":"application/activity+json",
"href":"https://altair.example.com/users/alice"},
{"rel":"http://ostatus.org/schema/1.0/subscribe",
"template":"https://altair.example.com/authorize_interaction?uri={uri}"}]}
"""

INTERESTING_HEADERS = {
        'received': {
            'content-type',
            'status',
            },
        'sent': {
            'accept',
            'status',
            },
        }

def register_uris():
    import httpretty

    PLAIN_JSON = 'application/json; charset=utf-8'

    httpretty.register_uri(
        httpretty.GET,
        "https://altair.example.com/.well-known/host-meta",
        body=EXAMPLE_HOST_META,
        content_type='application/xrd+xml; charset=utf-8',
        )

    httpretty.register_uri(
        httpretty.GET,
        "https://altair.example.com/.well-known/nodeinfo",
        body=EXAMPLE_NODEINFO_1,
        content_type = PLAIN_JSON,
        )

    httpretty.register_uri(
        httpretty.GET,
        "https://altair.example.com/nodeinfo/2.0",
        body=EXAMPLE_NODEINFO_2,
        content_type = PLAIN_JSON,
        )

    httpretty.register_uri(
        httpretty.GET,
        ('https://altair.example.com/.well-known/webfinger?'
         'resource=acct:alice@altair.example.com'),
        body=(
  '{"subject":"acct:alice@altair.example.com","aliases":["https://altair'
  '.example.com/@alice","https://altair.example.com/users/alice"],'
  '"links":[{"rel":'
  '"http://webfinger.net/rel/profile-page","type":"text/html","href":"h'
  'ttps://altair.example.com/@alice"},{"rel":"self","type":"application/act'
  'ivity+json","href":"https://altair.example.com/users/alice"},{"rel":"htt'
  'p://ostatus.org/schema/1.0/subscribe","template":"https://altair'
  '.example.com/authorize_interaction?uri={uri}"}]}'
  ),
        content_type='application/jrd+json; charset=utf-8',

  )

def fix_up_found(found):
    import json

    for line in found:
        if line['method'] in INTERESTING_HEADERS:
            line['headers'] = dict([
                (k,v) for k,v in
                line['headers'].items()
                if k.lower() in INTERESTING_HEADERS[line['method']]
                ])
        if line['method']=='received' and line.get('cached', None)==False:
            del line['cached']

        if line['method']=='received':
            is_json = 'json' in line['headers'].get('content-type','')
        else:
            # doing this so the set of things which can have encoded json
            # payloads is easily expanded
            is_json = False

        if is_json and isinstance(line['body'], str):
            line['body'] = json.loads(line['body'])

    return found
