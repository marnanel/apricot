import apricot
import httpretty
from test import *

def run_lookup(backend_class, constructor_kwargs,
               lookup_kwargs, expected):
    register_uris()
    found = []

    def outflow_for_testing(method, **kwargs):

        assert method in METHODS

        line = {'method': method}
        line |= kwargs
        found.append(line)

    backend = backend_class(
            outflow = outflow_for_testing,
            session_factory = apricot.cache.session_factory_with_no_cache,
            **constructor_kwargs,
            )

    backend.lookup(
            **lookup_kwargs,
            )

    assert fix_up_found(found)==expected, repr(backend_class)

@httpretty.activate(verbose=True, allow_net_connect=False)
def test_backend_host():
    run_lookup(
            backend_class = apricot.backend.Host,
            constructor_kwargs = {},
            lookup_kwargs = {
                'address': 'altair.example.com',
                },
            expected = [
                {'method': 'status', 'message': 'Sending request'},
                {'method': 'sent', 'headers': {
                    'Accept': 'application/xml',
                    },
                 'body': None,
                 'reason': 'host-meta',
                 'verb': 'GET',
                 'url': 'https://altair.example.com/.well-known/host-meta'
                 },
                {'method': 'received',
                 'headers': {
                     'content-type': 'application/xrd+xml; charset=utf-8',
                     'status': '200',
                     },
                 'body': EXAMPLE_HOST_META,
                 'reason': 'host-meta',
                 },
                {'method': 'results',
                 'webfinger': (
                     'https://altair.example.com/.well-known/'
                     'webfinger?resource={uri}'
                     ),
                 },
                ],
            )

@httpretty.activate(verbose=True, allow_net_connect=False)
def test_webfinger():
    run_lookup(
            backend_class = apricot.backend.Webfinger,
            constructor_kwargs = {},
            lookup_kwargs = {
                'address': 'alice@altair.example.com',
                },
            expected = [
                {'method': 'status',
                'message': 'Sending request'},
                {'method': 'sent',
                    'headers': {'Accept': 'application/xml'},
                    'body': None,
                    'verb': 'GET',
                    'url': 'https://altair.example.com/.well-known/host-meta',
                    'reason': 'host-meta'},
                {'method': 'received',
                    'headers': {'content-type': 'application/xrd+xml; charset=utf-8',
                        'status': '200'},
                    'body': '<?xml version="1.0" encoding="UTF-8"?>\n<XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0">\n  <Link rel="lrdd"\n  template="https://altair.example.com/.well-known/webfinger?resource={uri}"/>\n</XRD>',
                    'reason': 'host-meta'},
                {'method': 'status',
                    'wait': True,
                    'message': 'Sending request'},
                {'method': 'sent',
                    'headers': {'Accept': 'application/jrd+json'},
                    'body': None,
                    'verb': 'GET',
                    'url': 'https://altair.example.com/.well-known/webfinger?resource=alice@altair.example.com',
                    'reason': 'webfinger'},
                {'method': 'received',
                    'headers': {'content-type': 'application/jrd+json; charset=utf-8',
                        'status': '200'},
                    'body': {'subject': 'acct:alice@altair.example.com',
                        'aliases': ['https://altair.example.com/@alice',
                            'https://altair.example.com/users/alice'],
                        'links': [{'rel': 'http://webfinger.net/rel/profile-page',
                            'type': 'text/html',
                            'href': 'https://altair.example.com/@alice'},
                            {'rel': 'self',
                                'type': 'application/activity+json',
                                'href': 'https://altair.example.com/users/alice'},
                            {'rel': 'http://ostatus.org/schema/1.0/subscribe',
                                'template': 'https://altair.example.com/authorize_interaction?uri={uri}'}]},
                            'reason': 'webfinger'},
                {'method': 'results',
                    'html-profile': 'https://altair.example.com/@alice',
                    'activitypub': 'https://altair.example.com/users/alice',
                    'subject': 'acct:alice@altair.example.com',
                    'alias': ['https://altair.example.com/@alice',
                        'https://altair.example.com/users/alice'],
                    },
                ],
            )
